# Rigid body fit

Align a biolmolecule structure to a density.

## Installing and running

Install the latest release from the python package manager

```bash
pip3 install rigidbodyfit
```

Then run it

```bash
rigid-body-fit
```

## Installing and running from source (if you must)

Running directly from the shell

```bash
./rigid-body-fit.py
```

## Author

Christian Blau
